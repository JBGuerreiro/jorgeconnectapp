const fs = require('fs');
const childProcess = require('child_process');
const https = require('https');

childProcess.execSync('git clone git@bitbucket.org:adaptavistlabs/eslint-plugin-scriptrunner.git')
const createPRUrl = '/2.0/repositories/JBGuerreiro/jorgeconnectapp/pullrequests'
const getDefaultReviewersUrl = "/2.0/repositories/JBGuerreiro/jorgeconnectapp/default-reviewers"
const authorization = "Basic SkJHdWVycmVpcm86NDJVVkRXVUxOR3N4bVpFRlJFUlk=" //this needs to go to env vars
const version = childProcess.spawnSync('cd eslint-plugin-scriptrunner && git describe --tags $(git rev-list --tags --max-count=1)', {shell: true})

let versionLinting;

versionLinting = version.output.toString().replace(/(^,)|(,$)/g, "").trim()

const fileName = "test.json";
const fileData = fs.readFileSync(fileName, 'utf-8');
const JSONFileData = JSON.parse(fileData)
// //update key value

if(JSONFileData["linting-dep"] === `ssh://git@bitbucket.org:adaptavistlabs/eslint-plugin-scriptrunner.git#${versionLinting}`) return

JSONFileData["linting-dep"] = `ssh://git@bitbucket.org:adaptavistlabs/eslint-plugin-scriptrunner.git#${versionLinting}`

// //We have updated the content now let's write it back

fs.writeFileSync(fileName, JSON.stringify(JSONFileData, null, 4))

childProcess.execSync(`git add . && git commit -m "Update frontend linting dependency" && git push`, {stdio: "inherit"})

const makeRequest = (requestOptions, body) => {
    return new Promise((resolve, _) => {
        const request = https.request(requestOptions, res => {
            let chunk = '';

            res.on('data', data => {
                chunk += data;
            });
        
            res.on('end', () => {

                resolve(chunk);

            });
        });

        if(body) {
            request.write(body);
        }

        request.end();
    })
}

const fetchReviewersAndCreatePR = async () => {

const reqOptions = {
    hostname: 'api.bitbucket.org',
    port: 443,
    path: getDefaultReviewersUrl,
    method: 'GET',
};

// const response = await makeRequest(reqOptions)


// const reviewers = JSON.parse(response).values.map(value => value.uuid)
 const requestBody = {
         "title": "SRCLOUD-0-update-frontend-linting-dependency",
         "source": {
             "branch": {
                 "name": "SRCLOUD-0-Bump-linting-version"
             }
         },
         "destination": {
            "branch": {
                "name": "master"
            }
        },
         "close_source_branch": true
    }

     await makeRequest({
         ...reqOptions,
         method: 'POST',
         path: createPRUrl,
         headers: {
            'Authorization': authorization,
            'Content-Type': 'application/json'
         }
     }, JSON.stringify(requestBody))
}

fetchReviewersAndCreatePR()